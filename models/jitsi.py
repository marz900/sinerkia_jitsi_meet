from odoo import models, fields, api, _
from datetime import datetime
from random import choice
import pytz
import logging

logger = logging.getLogger(__name__)


def create_hash():
    size = 32
    values = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    p = ''
    p = p.join([choice(values) for i in range(size)])
    return p


class JistiMeet(models.Model):
    _name = 'sinerkia_jitsi_meet.jitsi_meet'
    _description = 'Jitsi Meeting'
    _order = 'date desc'

    def _get_default_participant(self):
        result = []
        result.append(self.env.user.id)
        # logger.info("\n---------------info-------------------\n{}\n".format(result))
        return [(6, 0, result)]

    name = fields.Char('Meeting Name', required=True)
    hash = fields.Char('Hash')
    date = fields.Datetime('Date', required=True)
    date_delay = fields.Float('Duration', required=True, default=1.0)
    participants = fields.Many2many('res.users', string='Participant', required=True)
    external_participants = fields.One2many('sinerkia_jitsi_meet.external_user', 'meet', string='External Participant')
    url = fields.Char(string='URL to Meeting', compute='_compute_url')
    calendar_event_id = fields.Many2one('calendar.event', string="Course Meeting")
    closed = fields.Boolean('Closed', default=False)
    current_user = fields.Many2one('res.users', compute='_get_current_user')

    @api.depends()
    def _get_current_user(self):
        for rec in self:
            rec.current_user = self.env.user

    @api.model
    def create(self, vals):

        vals['hash'] = create_hash()
        # logger.info("\n---------------info-------------------\n{}\n".format(vals))
        res = super(JistiMeet, self).create(vals)
        logger.info(res)
        return res

    @api.multi
    def action_close_meeting(self):
        self.write({'closed': True})

    @api.multi
    def action_reopen_meeting(self):
        self.write({'closed': False})

    @api.multi
    def open(self):
        return {'name': 'JITSI MEET',
                'res_model': 'ir.actions.act_url',
                'type': 'ir.actions.act_url',
                'target': 'new',
                'url': self.url
                }

    @api.model
    def _compute_url(self):
        config_url = self.env['ir.config_parameter'].get_param(
            'sinerkia_jitsi_meet.jitsi_meet_url',
            default='https://meet.jit.si/')
        for r in self:
            if r.hash and r.name:
                r.url = config_url + r.hash
                # logger.info("---------------info-------------------{}".format(r.url))

    # class CourseMeetingsLine(models.Model):
    #     _name = 'calendar.meetings.line'
    #     _inherit = ['mail.thread', 'mail.activity.mixin']
    #
    #     TYPES = [
    #         ('moderator', 'participants'),
    #         ('attendee', 'external_participants')
    #     ]
    #
    #     attendee = fields.Many2one("res.users", string="Attendee")
    #     attendee_type = fields.Selection(selection=TYPES, string="State", default='attendee')
    #     meeting_url = fields.Char(string="Joining Link")
    #
    #
    # @api.multi
    # def send_invitation(self, email_values):
    #
    #     template = self.env.ref('sinerkia_jitsi_meet.email_template_edi_jitsi_meet')
    #     logger.info("\n=====++== %s : %s ==++=====\n", self, template)
    #     mails_to_send = self.env['mail.mail']
    #     if template:
    #         for line in self:
    #             mail_id = template.send_mail(line.id)
    #             current_mail = self.env['mail.mail'].browse(mail_id)
    #             current_mail.mail_message_id.write(email_values)
    #             mails_to_send |= current_mail
    #         res = mails_to_send.send()
    #         logger.info("---------------info-------------------{}".format(res))


class JitsiMeetExternalParticipant(models.Model):
    _name = 'sinerkia_jitsi_meet.external_user'
    _description = 'Jitsi Meeting External Participant'
    _order = 'name'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char('Email')
    meet = fields.Many2one('sinerkia_jitsi_meet.jitsi_meet', string='Meeting')
    partner_id = fields.Many2one(related='meet.create_uid.partner_id')
    meeting_date = fields.Datetime(related='meet.date', string='Meeting Date')
    meeting_name = fields.Char(related='meet.name', string='Meeting Name')
    meeting_url = fields.Char(related='meet.url', string='Meeting URL')
    send_mail = fields.Boolean('Send Invitation', default=True)
    mail_sent = fields.Boolean('Invitation Sent', readonly=True, default=True)
    calendar_event_id = fields.Many2one("calendar.event", string="Course Meeting")
    date_formated = fields.Char(compute='_format_date')

    def _format_date(self):

        local = pytz.timezone(self._context.get('tz') or 'UTC')
        for part in self:
            part.date_formated = datetime.strftime(
                pytz.utc.localize(part.meeting_date).astimezone(local),
                "%d/%m/%Y, %H:%M:%S")
            # part.date_formated = fields.Datetime.from_string(part.meeting_date).strftime('%m/%d/%Y, %H:%M:%S')

    @api.model
    def create(self, vals):

        # logger.info("\n---------------info-------------------\n{}\n".format(vals))
        res = super(JitsiMeetExternalParticipant, self).create(vals)
        # logger.info("---------------send mail-------------------{}".format(res.send_mail))
        if res.send_mail:
            template = self.env.ref('sinerkia_jitsi_meet.email_template_edi_jitsi_meet')
            # logger.info("---------------vérifi-------------------{}".format(template))
            self.env['mail.template'].browse(template.id).send_mail(res.id)
            res.sudo().write({'mail_sent': True})

        return res

    @api.multi
    def write(self, email_values):

        self.ensure_one()
        if 'send_mail' in email_values and email_values.get('send_mail'):
            if not self.mail_sent:
                template = self.env.ref('sinerkia_jitsi_meet.email_template_edi_jitsi_meet')
                logger.info("---------------info-------------------{}".format(template))
                if template:
                    log = self.env['mail.template'].browse(template.id).send_mail(_.id, email_values=email_values,
                                                                                  force_send=True)
                    logger.info("---------------info-------------------{}".format(log))
                    email_values.update({'mail_sent': True})

    @api.multi
    def send_invitation(self, email_values):

        template = self.env.ref('sinerkia_jitsi_meet.email_template_edi_jitsi_meet')
        logger.info("\n=====++== %s : %s ==++=====\n", self, template)
        mails_to_send = self.env['mail.mail']
        if template:
            for line in self:
                mail_id = template.send_mail(line.id)
                current_mail = self.env['mail.mail'].browse(mail_id)
                current_mail.mail_message_id.write(email_values)
                mails_to_send |= current_mail
            res = mails_to_send.send()
            logger.info("---------------info-------------------{}".format(res))
