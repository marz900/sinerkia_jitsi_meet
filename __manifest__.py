# -*- coding: utf-8 -*-
# © 2020     Sinerkia iD (<https://www.sinerkia.com>).
{
    'name': 'Sinerkia Jitsi Meet Integration',
    'version': '12.0.1.0.2',
    'category': 'Extra Tools',
    'sequence': 1,
    'summary': 'Créer et partager Jitsi Rencontrez des vidéoconférences avec d’autres utilisateurs et partenaires externes',
    'description': """
		Ajoute une nouvelle APPLICATION pour créer et partager des réunions de vidéoconférence Jisti Meet entre les utilisateurs d’Odoo. Vous pouvez inviter des utilisateurs externes en envoyant du courrier d’Odoo.
        Lorsque vous rejoignez la réunion Odoo ouvre un nouvel onglet navigateur afin que vous puissiez continuer à travailler sur Odoo, et partager l’écran avec vos partenaires à Jisti Meet. 
    """,
    "author": "Sinerkia ID",
    "website": "https://www.sinerkia.com",
    "depends": ['base','web','mail'],
    "data": [
    'security/base_security.xml','views/sinerkia_jitsi_meet_views.xml','data/sinerkia_jitsi_meet.xml','data/mail_template.xml','security/ir.model.access.csv','security/base_security.xml',
    ],
    'images': ['images/main_screenshot.png'],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'AGPL-3',
}
